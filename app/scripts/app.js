angular.module('Presentation', ['ngMobile', 'Core'])

          // Named routes instead of route provider
.config(['$namedRouteProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(false);

  $routeProvider
    .when('/:colour/:slide', {
      templateUrl: 'views/introduction.html',
      name: 'introduction'
    })
    .when('/:colour/history/:slide', {
      templateUrl: 'views/history.html',
      name: 'history'
    })
    .when('/:colour/routing/:slide', {
      templateUrl: 'views/routing.html',
      name: 'routing'
    })
    .when('/:colour/gestures/:slide', {
      templateUrl: 'views/gestures.html',
      name: 'gestures'
    })
    .when('/:colour/examples/:slide', {
      templateUrl: 'views/examples.html',
      name: 'examples'
    })
    .otherwise({
      redirectTo: '/green/1'    // The default colour
    });
}]).

controller('PresentCtrl', ['$window', '$scope', '$route', '$routeParams', function (window, scope, route, params) {

  var presentation = [
      {name: 'introduction', slides: 4},
      {name: 'history', slides: 3},
      {name: 'routing', slides: 2},
      {name: 'gestures', slides: 3},
      {name: 'examples', slides: 2}
    ],
    sectionIndex = 0;

  ///
  // Keep track of the colour
  scope.colours = ['green', 'blue', 'red'];
  scope.$watch('backColour', function(newVal) {
    if (newVal !== params.colour) {
      route.to[route.current.name]({colour: newVal});
    }
  });
  scope.$on('$routeChangeSuccess', function (ev, current) {
    scope.backColour = params.colour;
    scope.slide = parseInt(params.slide, 10);
    if (presentation[sectionIndex].name != route.current.name) {
      angular.forEach(presentation, function(value, key) {
        if (value.name === route.current.name) {
          sectionIndex = key;
        }
      });
    }
  });

  ///
  // Move to previous slide
  scope.previous = function() {
    var moveTo = scope.slide - 1;

    if (moveTo < 1 && sectionIndex > 0) {
      sectionIndex -= 1;
      route.to[presentation[sectionIndex].name]({
        slide: presentation[sectionIndex].slides, // The last slide in the section
        colour: scope.colours[sectionIndex % 3]   // A new colour for each section
      });
    } else if (moveTo > 0) {
      route.to[route.current.name]({
        slide: moveTo // The previous slide in the current section
      });
    }
  };

  ///
  // Move to next slide
  scope.next = function() {
    var moveTo = scope.slide + 1;

    if (moveTo > presentation[sectionIndex].slides && sectionIndex < (presentation.length - 1)) {
      sectionIndex += 1;
      route.to[presentation[sectionIndex].name]({
        slide: 1,   // The first slide in the new section
        colour: scope.colours[sectionIndex % 3]
      });
    } else if (moveTo <= presentation[sectionIndex].slides) {
      route.to[route.current.name]({
        slide: moveTo
      });
    }
  };

}]).

// Sliders for colour cahnge
directive('changeColour', function() {
  return function (scope, iElement, iAttrs) {
    var red = angular.element('.red-range').scope(),
        green = angular.element('.green-range').scope(),
        blue = angular.element('.blue-range').scope(),
        drag = angular.element('div.drag-example'),
        setColours = function() {
          drag.css('background-color', 'rgb(' + red.value + ',' + green.value + ',' + blue.value + ')');
        };

      red.$watch('userUpdate', setColours);
      green.$watch('userUpdate', setColours);
      blue.$watch('userUpdate', setColours);
  }
});
