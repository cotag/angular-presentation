'use strict';

describe('Service(Core): Storage', function () {

	// load the module we are testing
	beforeEach(module('Core'));

	var storage;

	// Initialize the objects we are testing
	beforeEach(inject(function ($storage) {
		storage = $storage;
	}));

	afterEach(function() {
		storage.clear();
	});


	it('should be able to store strings', function () {
		storage.put('somekey', 'simple data');
		expect(storage.get('somekey')).toBe('simple data');
	});

	it('should be able to store arrays', function () {
		storage.put('somekey', [1,2,3]);
		expect(storage.get('somekey')).toEqual([1,2,3]);
	});

	it('should be able to store objects', function () {
		storage.put('somekey', {me: 'steve'});
		expect(storage.get('somekey')).toEqual({me: 'steve'});
	});

	it('should be able to remove an item from the store', function () {
		storage.put('object', {me: 'steve'});
		storage.put('array', [1,2,3]);
		storage.put('string', 'simple data');
		expect(storage.get('object')).toEqual({me: 'steve'});
		expect(storage.get('array')).toEqual([1,2,3]);
		expect(storage.get('string')).toBe('simple data');
		storage.remove('array');
		expect(storage.get('object')).toEqual({me: 'steve'});
		expect(storage.get('array')).toBe(undefined);
		expect(storage.get('string')).toBe('simple data');
	});

	it('should be able to clear all items from a store', function () {
		storage.put('object', {me: 'steve'});
		storage.put('array', [1,2,3]);
		storage.put('string', 'simple data');
		expect(storage.get('object')).toEqual({me: 'steve'});
		expect(storage.get('array')).toEqual([1,2,3]);
		expect(storage.get('string')).toBe('simple data');
		storage.clear();
		expect(storage.get('object')).toBe(undefined);
		expect(storage.get('array')).toBe(undefined);
		expect(storage.get('string')).toBe(undefined);
	});
});
